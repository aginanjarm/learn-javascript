// immedieately invoke function expression

var fruit = 'Apple';

(function(){
    var fruit = 'Orange';
    console.log(fruit); // this value will invoked immedieately
}());

console.log(fruit);
