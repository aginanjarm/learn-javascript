'use strict';

// Simple callback
let simplecaller = () => {
    console.log('I am running inside function (callback)')
}

let triggerFunction = (callback) => {
    console.log('Hi World!')

    callback()
}

triggerFunction(simplecaller)
triggerFunction(() => console.log('Another Callback~~'))

console.log(`------------`)
// Another example
// First attemp
let calc1 = (num1, num2, type) => {
    if(type === 'add'){
        return num1 + num2
    } else if ( type === 'multiply') {
        return num1 * num2
    }
}

console.log(calc1(2,2, 'add'))

// Refactoring
let add = (a,b) => {
    return a+b
}, 
multiply = (a,b) => {
    return a*b
},
print = (a, b) => {
    return `Parameters are : ${a} and ${b}`
}


let calc2 = (num1, num2, callback) => {
    return callback(num1, num2)
}

console.log(calc2(2,1, add))
console.log(calc2(2,3, multiply))
console.log(calc2(2,5, print))
console.log(calc2(1,2, function(a,b) {
    return a-b
}))

console.log(`------------`)
// Another example
let myArr = [
    {
        num: 1,
        str: 'apple'
    },
    {
        num: 3,
        str: 'cabbage'
    },
    {
        num: 2,
        str: 'ban'
    },

];

myArr.sort(function(val1, val2){
    if(val1.num < val2.num) {
        return -1;
    }
    return 1;
})

console.log(myArr)