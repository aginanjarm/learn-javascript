// function statement
function biology() {
    console.log('Biology book.');
}

// function are first-class 
function printBook(fn){
    fn();
}
printBook(biology);

// function expression
var tech = function(){
    console.log('Tech book.')
};
printBook(tech);

printBook(function(){
    console.log('Finance book.');
})