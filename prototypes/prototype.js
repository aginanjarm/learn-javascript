// Example non prototype
var Car = function(){
    this.color = 'red';
    this.automatic = true;

    this.run = function(power) {
        if(power) {
            console.log( 'Car is running.' );
        } else {
            console.log( 'Car is off.' );
        }

    }
};

var toyota = new Car();
toyota.run(true);

// Example prototype
var Bus = function(){
    this.color = 'white';
    this.automatic = false;
};

Bus.prototype.run = function(power) {
    if(power) {
        console.log( 'Bus is running.' );
    } else {
        console.log( 'Bus is off.' );
    }

}
var primajasa = new Bus();
console.log(primajasa);
primajasa.run(true);