// Base class
var Father = function(name, age) {
    this.name = name;

    if(name === undefined){
        this.name = 'Ginanjar';
    }
    
    this.gender = 'Male';

    this.age = age;
    if(age === undefined){
        this.age = '30';
    }
};

Father.prototype.print = function() {
    console.log('Name of father is : '+this.name+'\nAnd his age is : '+this.age);
    console.log('--- --- ---');
}

var father01 = new Father();
father01.print();

// Another Father
const father02 = new Father('Gaha', 34);
delete father02.age;
father02.print();

const father03 = new Father('Bill Gates', 64);
father03.print();


// Sub class
var Son = function(name, age){
    
    Father.call(this);

    if(name !== undefined){
        this.name = name;
    }
    
    this.gender = 'Male';

    if(age !== undefined){
        this.age = age;
    }
};

Son.prototype = Object.create(Father.prototype);
Son.prototype.constructor = Son;


var son01 = new Son('Ayyubi', 5);

Son.prototype.print = function() {
    console.log('Name of son is : '+this.name+'\nAnd his age is : '+this.age);
};

son01.print();
