const simpleEmitter = require('./simpleEmitter01')

const emtr = new simpleEmitter()
emtr.on('sapa', ()=>{
    console.log('halo, ini saya lagi menjalankan event emitter')
})

emtr.on('sapa', ()=>{
    console.log('ini manggil event \'sapa\' yang ke dua..')
})

console.log('----- ./.. -----')
emtr.emit('sapa')