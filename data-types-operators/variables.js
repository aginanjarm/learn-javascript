"use strict"
// Difference between var, let, const
var testVar = 11
console.log(testVar)
testVar = 22
console.log(testVar)
console.log('---------');

var funcTestLet = () => {
    let testLet = 33
    console.log(testLet + ": inside function scope.")
    if(true) {
        let testLet = 44
        console.log(testLet + ": this value was changed.")
        console.log('---------')
    }

    console.log(testLet + ": ")
    console.log('---------')
}
funcTestLet()

// const testConst = 55
// console.log(testConst)

// const testConst = 66
// console.log(testConst)


function func1() {
    // if (true) {
    //     const tmp = 123;
    // }
    // console.log(tmp); // ReferenceError: tmp is not defined
}
// In contrast, var-declared variables are function-scoped:
func1()

function func() {
    if (true) {
        var tmp = 123;
    }
    console.log(tmp); // 123
}
func()