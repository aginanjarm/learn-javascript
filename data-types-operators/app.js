// Numeric types
console.log(123);
console.log("---");

// String
console.log("Hello Javascript");
console.log("First line\nSecond line\nThird line\nN+1 line");
console.log("---");

// Undefined
console.log(undefined);
console.log("---");

// Arithmetic
console.log("x*y*z="+(10000*2999999900*398778) );
console.log("x+y="+(1+2) );
console.log("---");

// Comparison
console.log("1 > 0 "+ (1 > 0));
console.log("1 < 0 "+ (1 < 0));
console.log("1 == 0 "+ (1 == 0));
console.log("1 >= 0 "+ (1 >= 0));
console.log("1 <= 0 "+ (1 <= 0));
console.log("1 != 0 "+ (1 != 0));
console.log("1 === 0 "+ (1 === 0));
console.log("1 != 0 "+ (1 !== 0));

// Scopes
console.log('-- Scopes chapter --');
var expr = 'global value';
var fn = function() {
    var expr = 'local value';

    return expr;
};

console.log(expr);
console.log(fn());

